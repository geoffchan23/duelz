var app = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);

// Socket routes
var game = require('./game/game.js');
var player = require('./player/player.js');

