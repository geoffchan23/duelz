'use strict';
var app = require('express');
var cors = require('cors');

// Set up server
var http = require('http').Server(app);
var io = require('socket.io').listen(http);

// Import socket routes
// require('./sockets');
var SOCKET_LIST = {};
var id = 1;
var x = 100;
var y = 100;

io.on('connection', function (socket) {
	socket.id = new Date().getUTCMilliseconds();	
	SOCKET_LIST[socket.id] = socket;

	socket.on('ready',function(){
		console.log('CONNECTION MADE', socket.id, Object.keys(SOCKET_LIST));
		socket.emit('connected', { id: socket.id });
	});

	socket.on('disconnect',function(){
		delete SOCKET_LIST[socket.id];
		console.log(socket.id, 'disconnected', Object.keys(SOCKET_LIST));				
		io.emit('delete player', socket.player);
	});

  socket.on('new player', (data) => {
		socket.player = {
			id: socket.id,
			x: data.x,
			y: data.y
		}
		// id++;
	
		// console.log('new player created', socket.player.id);

		socket.emit('allplayers', getAllPlayers());					
		io.emit('new player created', socket.player);	
	});

	socket.on('move player', (data) => {
		if (SOCKET_LIST[data.id]) {
			SOCKET_LIST[data.id].player.x = data.x;
			SOCKET_LIST[data.id].player.y = data.y;
			SOCKET_LIST[data.id].player.angle = data.angle;
		}
	});

});

function getAllPlayers() {
	var players = [];
	Object.keys(SOCKET_LIST).forEach(function(socketID){
		var player = SOCKET_LIST[socketID].player;
		if (player) {
			players.push(player);
		}
	});
	return players;
}

setInterval(function() {
	var pack = [];

	for (var i in SOCKET_LIST){
		var socket = SOCKET_LIST[i];

		if (socket.player) {
			pack.push({
				x: socket.player.x,
				y: socket.player.y,
				id: socket.player.id,
				angle: socket.player.angle
			});    
		}
	}
	for (var i in SOCKET_LIST){
		var socket = SOCKET_LIST[i];
		if (socket) socket.emit('newPositions', pack);
	}
}, 5);




http.listen(3000, function () {
  console.log('Server listening on port 3000...');
});
