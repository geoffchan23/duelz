class Player extends Phaser.Sprite {
	constructor(game, x, y, name, isControlledByClient, socket, id) {
		super(game, x, y, name);
		this.game = game;		
		this.socket = socket;

		this.anchor.setTo(0.5, 0.5);

		this.id = id;
		this.health = 10;
		this.bullets = this.addBullets(30);
		this.isControlledByClient = isControlledByClient;
		this.speed = 300;
		this.strafeVelocity = 4;

		this.game.add.existing(this);
		this.scale.setTo(0.15,0.15);
		this.game.physics.enable(this);

		this.game.input.addMoveCallback((pointer, x, y) => {
			var pointerCoords = [pointer.x, pointer.y];
			this.angle = this.getPlayerAngle(this, pointerCoords);
		});
	}

	update() {

		if (this.isControlledByClient) {
			/**
			 * Player1 Controls
			 */
			if (this.game.input.keyboard.isDown(Phaser.Keyboard.A))
			{
					this.game.physics.arcade.velocityFromAngle(this.angle + 180, this.speed, this.body.velocity);
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });
			}
			else if (this.game.input.keyboard.isDown(Phaser.Keyboard.D))
			{
					this.game.physics.arcade.velocityFromAngle(this.angle, this.speed, this.body.velocity);
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });
			}
			else {
					this.body.velocity.x = 0;
					this.body.velocity.y = 0;
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });
			}

			if (this.game.input.keyboard.isDown(Phaser.Keyboard.W) || this.game.input.keyboard.isDown(Phaser.Keyboard.UP))
			{
					this.game.physics.arcade.velocityFromAngle(this.angle - 90, this.speed, this.body.velocity);
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });
			} 
			else if (this.game.input.keyboard.isDown(Phaser.Keyboard.S) || this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN))
			{
					this.game.physics.arcade.velocityFromAngle(this.angle + 90, this.speed, this.body.velocity);
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });
			} 
			if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
			{
					this.angle-=5;
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });				
			} else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
			{
					this.angle+=5;
					this.socket.emit('move player', { x: this.x, y: this.y, angle: this.angle, id: this.id });		
			} 

			if (this.game.input.activePointer.leftButton.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
			{
					// this.shootBullet(this);
					// this.socket.emit('shoot bullet', { x: this.x, y: this.y, angle: this.angle, playerId: this.id });
			}
		}
	}

	addBullets(num) {
		var bullets = this.game.add.group();
		bullets.createMultiple(num, 'bullet');
		bullets.setAll('anchor.x', 0.5);
		bullets.setAll('anchor.y', 0.5);
		bullets.setAll('outOfBoundsKill', true);
		bullets.setAll('checkWorldbounds', true);
		this.game.physics.enable(bullets);
		return bullets;
	}

	showHealth() {
		this.healthText = this.game.add.text(this.game.width -50, this.game.height-this.game.height+10, this.health);
		// Font style
		this.healthText.font = 'Arial Black';
		this.healthText.fontSize = 30;
		this.healthText.fill = '#ff6600';
	}

	getPlayerAngle(player, pointerCoords) {
		var targetAngle = (360 / (2 * Math.PI)) * this.game.math.angleBetween(
			player.x, player.y, ...pointerCoords) + 90;

		if (targetAngle < 0) targetAngle += 360;

		return targetAngle;
	}
}

export default Player;