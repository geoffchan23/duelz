class Preloader extends Phaser.State {

  constructor() {
    super();
    this.asset = null;
    this.ready = false;
  }

  preload() {
		console.log('preloader-preload()');
    //setup loading bar
    this.asset = this.add.sprite(this.game.width * 0.5 - 110, this.game.height * 0.5 - 10, 'preloader');
    this.load.setPreloadSprite(this.asset);

    //Setup loading and its events
    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
		this.loadResources();
  }

  update() {
		console.log('preloader-update()');
      // if (this.ready) {
        this.game.state.start('menu');
      // }
  }

  loadResources() {
		console.log('preloader-loadResources()');
      // load your resources here
  }

  onLoadComplete() {
		console.log('preloader-onLoadComplete()');
    this.ready = true;
  }
}

export default Preloader;
