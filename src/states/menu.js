class Menu extends Phaser.State {

  constructor() {
    super();
  }
  
  create() {
		console.log('menu-create()');
    var text = this.add.text(this.game.width * 0.5, this.game.height * 0.5, 'CLICK TO START', {
      font: '42px Arial', fill: '#ffffff', align: 'center'
    });
    text.anchor.set(0.5);

    this.input.onDown.add(this.startGame, this);
  }

  update() {
		console.log('menu-update()');
	}

  startGame () {
		console.log('menu-starGame()');
    this.game.state.start('game');
  }

}

export default Menu;
