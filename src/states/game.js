import Player from './../models/Player';
import io from '../../node_modules/socket.io-client/dist/socket.io.js';

const Client = {};

class Game extends Phaser.State {

  constructor() {
		super();
		
		this.shootTime = 0;
		this.dragging = false;
		this.movement = {};
		this.movement.movingForward     = false;
		this.movement.movingBackward    = false;
		this.movement.moving            = false;
		this.playerCount = 0;
		this.models = [];
		this.bullet;
		this.playerMap = {};
		this.playerId;
	}
	
	preload() {
		console.log('game-preload()');
		
		this.load.image('herb', 'assets/ships/warbird.png');
		this.load.image('bob', 'assets/ships/t-fighter.png');
		this.load.image('bullet', 'assets/bullet.png');

		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.game.physics.arcade.gravity.y = 0;
	}

  create() {
		console.log('game-create()');

		Client.socket = io.connect(':3000');

		this.playerMap = {};
		
		//Once socket is connected it will send up a player ID
		Client.socket.on('connected', (data) => {
			console.log('connected!', data);
			this.playerId = data.id
		});

		Client.socket.emit('ready');

		//Tells the server a new player joined
		Client.socket.emit('new player', { x: this.game.width*.5, y: this.game.height*.5 });
		
		//Creates new players as they join
		Client.socket.on('new player created', (data) => {
			console.log('create new bob', data);
			if (data.id != this.playerId) this.addPlayer('bob', data.id, data.x, data.y, false);
		});

		//Creates all of the players currently connected when the user logs in
		Client.socket.on('allplayers', (data) => {
			console.log('create all bobs', data);

			for (var i = 0; i < data.length; i++){
				if (data[i].id == this.playerId) {
					this.addPlayer('ghost', 'ghost', this.game.width*.5, this.game.height*.5, true);
					this.player1 = this.addPlayer('herb', this.playerId, this.game.width*.5, this.game.height*.5, true);
				} else {
					this.addPlayer('bob', data[i].id, data[i].x, data[i].y, false);					
				}
			}
		});
		
		//Runs 24 times per second to update player positions
		Client.socket.on('newPositions', (data) => {
			// console.log('player move event received', data);
			for (var i = 0; i < data.length; i++) {
				if (this.playerId != data[i].id && this.playerMap[data[i].id]) {
					this.playerMap[data[i].id].x = data[i].x;
					this.playerMap[data[i].id].y = data[i].y;
					this.playerMap[data[i].id].angle = data[i].angle;
				}
			}
		});

		//Removes players that disconnect
		Client.socket.on('delete player', (data) => {
			console.log('delete player', data);
			this.playerMap[data.id].destroy();
			delete this.playerMap[data.id];
		});

		this.game.input.keyboard.addKeyCapture([
			Phaser.Keyboard.LEFT,
			Phaser.Keyboard.RIGHT,
			Phaser.Keyboard.UP,
			Phaser.Keyboard.DOWN,
			Phaser.Keyboard.W,
			Phaser.Keyboard.S,
			Phaser.Keyboard.A,
			Phaser.Keyboard.D,
			Phaser.Keyboard.SPACEBAR
		]);
  }

  update() {
		// console.log('game-update()');

		// this.game.physics.arcade.overlap(this.player1.bullets, this.player2, this.collisionHandler, null, this);
		if (this.game.input.activePointer.leftButton.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
		{
				// console.log(this.playerMap[this.playerId].bullets);
				this.shootBullet(this.playerMap[this.playerId]);
				// this.socket.emit('shoot bullet', { x: this.x, y: this.y, angle: this.angle, playerId: this.id });
		}
	}
	
	addPlayer(name, id, x, y, isControlledByClient) {
		console.log('adding new player', name, id, x, y, isControlledByClient);
		var player = new Player(this.game, x, y, name, isControlledByClient, Client.socket, id);
		
		this.playerMap[id] = player;
		this.playerCount++;
		console.log(this.playerMap, this.playerCount);
		return player;
	}

	shootBullet(player) {
		if (this.time.now > this.shootTime) {
			var bullet = player.bullets.getFirstExists(false);

			if (bullet) {
				bullet.reset(player.x, player.y);
				this.game.physics.arcade.velocityFromRotation(player.rotation-1.5, 1000, bullet.body.velocity);
			}

			this.shootTime = this.time.now + 200;
			return bullet;
		}
	}

	checkOverlap(spriteA, spriteB) {
		if (!spriteA || !spriteB) return false;

    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
		
    return Phaser.Rectangle.intersects(boundsA, boundsB);
	}

	collisionHandler(player, bullet) {
		console.log('HIT!', bullet, player);

		if (player.health > 0) {
			player.damage(1);
			bullet.kill();
			player.healthText.setText(player.health);
		}
	}

  endGame() {
		console.log('game-endGame()');
    this.game.state.start('gameover');
  }

}

export default Game;
